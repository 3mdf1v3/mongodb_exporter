# Prometheus MongoDb exporter
Service install:
```
su - prometheus
mkdir mongodb_expoter

cd /etc/systemd/system/
vim mongodb_expoter.service

	[Unit]
	Description=RSA MongoDB Esm Exporter
	Wants=network-online.target
	After=network-online.target

	[Service]
	User=prometheus
	ExecStart=/home/prometheus/mongodb_expoter/mongodb_expoter -mongoDbUsername ${mongoDbUsername} -mongoDbPassword ${mongoDbPassword} -mongoDbAuthSource ${mongoDbAuthSource} -mongoDbHostURI ${mongoDbHostURI}

	[Install]
	WantedBy=default.target

systemctl daemon-reload
systemctl start mongodb_expoter
systemctl enable mongodb_expoter
```
